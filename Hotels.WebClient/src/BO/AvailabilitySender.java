/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package BO;

import Communicator.TCPTransmitter;
import General.Content;
import General.Header;
import General.Request;
import General.Response;
import General.TransactionType;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pr
 */
public class AvailabilitySender {

    private final String city;

    public AvailabilitySender(String city) {
        this.city = city;
    }

    public Response Send() {
        Header header = new Header();
        header.setTransactionType(TransactionType.AVAILABILITY);

        Content content = new Content();
        content.setCity(city);

        Request request = new Request();
        request.setHeader(header);
        request.setContent(content);

        TCPTransmitter transmitter = new TCPTransmitter("localhost", 9000);
        try {
            transmitter.Connect();
            Response response = transmitter.SendReceive(request);
            return response;
        } catch (IOException | ClassNotFoundException ex) {
        }

        return new Response();
    }
}
