/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package ApiLevel2;

import General.Response;

/**
 *
 * @author pr
 */
public interface ITransaction {

    Response ProcessTransaction();
}
