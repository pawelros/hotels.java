/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */

package General;

/**
 *
 * @author pr
 */
public enum TransactionType {
    AVAILABILITY,
    PRICING,
    BOOK
}
