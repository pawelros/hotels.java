/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */

package Interfaces;

/**
 *
 * @author pr
 */
public interface ILogger {
    public void writeDebug(String message);
    public void writeException(Exception ex);
}
