/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package General;

import java.io.Serializable;

/**
 *
 * @author pr
 */
public class Header implements Serializable {

    private TransactionType transactionType;

    /**
     * @return the transactionType
     */
    public TransactionType getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

}
