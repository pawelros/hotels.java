/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package ApiLevel3;

import General.Content;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author pr
 */
public class HotelsRetriever {

    String dbURL = "jdbc:derby://localhost:1527/Hotels";
    String user = "test";
    String pass = "test";

    public ArrayList<Content> RetrieveByCity(String city) {

        ArrayList<Content> hotelCollection = new ArrayList<>();

        try {
            //Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            Connection conn = DriverManager.getConnection(dbURL, user, pass);

            try (Statement stmt = conn.createStatement(); ResultSet results = stmt.executeQuery("select CODE,PRICE from TEST.HOTELS WHERE CITY LIKE '" + city + "'")) {
                
                while (results.next()) {
                    
                    Content hotel = new Content();
                    hotel.setCity(city);
                    hotel.setHotelCode(results.getString(1));
                    hotel.setPrice(results.getInt(2));
                    
                    hotelCollection.add(hotel);
                }
                
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return hotelCollection;
    }
}
