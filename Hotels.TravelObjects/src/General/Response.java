/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */

package General;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pr
 */
public class Response extends Request {
    private final List<String> errors;
    private List<Content> supplierResponses;
    
    public Response(){
        this.errors = new ArrayList<>();
        this.supplierResponses = new ArrayList<>();
    }
    
    public String[] getErrors() {
        return this.errors.toArray(new String[errors.size()]);
    }
    
    public void addError(String message) {
        this.errors.add(message);
    }

    public List<Content> getSupplierResponses() {
        return supplierResponses;
    }

    public void setSupplierResponses(List<Content> supplierResponses) {
        this.supplierResponses = supplierResponses;
    }
        
    
}
