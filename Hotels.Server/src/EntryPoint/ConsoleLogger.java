package EntryPoint;

/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */

import Interfaces.ILogger;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author pr
 */
public class ConsoleLogger implements ILogger {

    private final SimpleDateFormat dateFormatter;
    private static final String dateFormat = "[yyyy.MM.dd HH:mm:ss]";

    public ConsoleLogger() {
        this.dateFormatter = new SimpleDateFormat(dateFormat);
    }

    /**
     *
     * Public Methods
     * @param message
     */
    @Override
    public synchronized void writeDebug(String message) {
        System.out.printf("%s %s\r\n", dateFormatter.format(new Date()), message);
    }

    @Override
    public synchronized void writeException(Exception ex) {
        System.out.println(ex.getMessage());
        System.out.printf("%s %s\r\n%s", dateFormatter.format(new Date()), ex.getMessage(), getStackTrace(ex));
    }

    /**
     *
     * Private Methods
     */
    private static String getStackTrace(final Throwable throwable) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        return sw.getBuffer().toString();
    }
}
