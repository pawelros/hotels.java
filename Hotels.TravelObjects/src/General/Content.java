/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */

package General;

import java.io.Serializable;

/**
 *
 * @author pr
 */
public class Content implements Serializable {
    private String city;
    private String hotelCode;
    private Integer price;
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String HotelCode) {
        this.hotelCode = HotelCode;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer Price) {
        this.price = Price;
    }
}
