/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package Communicator;

import Communicator.SocketProcessor;
import Interfaces.ILogger;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 *
 * @author pr
 */
public class SocketHandler {

    private final ThreadPoolExecutor threadPool;
    private final ILogger logger;

    public SocketHandler(ILogger logger) {
        this.threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        this.logger = logger;
    }

    public void Handle(Socket socket) {
        //create new instance of SocketProcessor and put processing into thread pool
        //in order to process each incoming socket in a separate thread

        this.threadPool.execute(
                new SocketProcessor(this.logger)
                .Process(socket));

        LogDebug();
    }

    private void LogDebug() {
        this.logger.writeDebug("Processing new socket...");
        this.logger.writeDebug("Thread pool info:");
        this.logger.writeDebug(String.format("%-35s", "| active threads:") + "| " + this.threadPool.getActiveCount() + " |");
        this.logger.writeDebug(String.format("%-35s", "| max threads simultaneously used") + "| " + this.threadPool.getLargestPoolSize() + " |");
        this.logger.writeDebug(String.format("%-35s", "| completed tasks") + "| " + this.threadPool.getCompletedTaskCount() + " |");
    }
}
