/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package PerformanceTester;

import BO.AvailabilitySender;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author pr
 */
public class Tester {

    static String[] cities = new String[]{
        "Warsaw", "New York City", "London"
    };

    public static void main(String[] args) {

        List<Runnable> actionsToRun = new ArrayList<>();
        Random generator = new Random();

        for (int i = 0; i < 10000; i++) {
            String city = cities[generator.nextInt(2)];
            
            Runnable action = CreateAction(city);
            actionsToRun.add(action);
        }

        for(Runnable action : actionsToRun)
        {
            new Thread(action).start();
        }

    }

    private static Runnable CreateAction(final String city) {
        return new Runnable() {

            @Override
            public void run() {

                AvailabilitySender availabilitySender = new AvailabilitySender(city);
                availabilitySender.Send();
            }
        };
    }
}
