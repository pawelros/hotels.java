/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package Communicator;

import General.Request;
import General.Response;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author pr
 */
public class TCPTransmitter {

    private Socket socket;
    private final String destinationAddress;
    private final int destinationPort;

    public TCPTransmitter(String destinationAddress, int destinationPort) {
        this.destinationAddress = destinationAddress;
        this.destinationPort = destinationPort;
    }

    public void Connect() throws IOException {
        this.socket = new Socket(this.destinationAddress, this.destinationPort);
    }

    public Response SendReceive(Request request) throws ClassNotFoundException {

        Response response = new Response();

        try {
            ObjectOutputStream out = SocketUtils.getObjectOutputStream(this.socket);
            out.writeObject(request);

            ObjectInputStream in = SocketUtils.getObjectInputStream(this.socket);
            Object responseObject = in.readObject();

            if (responseObject instanceof Response) {
                response = (Response) responseObject;
            } else {
                response.addError("Server returned invalid response");
            }

        } catch (IOException ex) {
            response.addError(ex.getMessage());
        }

        return response;
    }
}
