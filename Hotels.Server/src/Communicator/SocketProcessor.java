/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package Communicator;

import ApiLevel1.TransactionDispatcher;
import General.Request;
import General.Response;
import Interfaces.ILogger;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author pr
 */
public class SocketProcessor {

    private final ILogger logger;

    public SocketProcessor(ILogger logger) {
        this.logger = logger;
    }

    public Runnable Process(final Socket socket) {
        return new Runnable() {

            @Override
            public void run() {
                Response response = new Response();

                try {
                    ObjectInputStream in = SocketUtils.getObjectInputStream(socket);
                    Object requestObject = in.readObject();
                    
                    Request request;
                    if (requestObject instanceof Request) {
                        request = (Request) requestObject;
                        logger.writeDebug("Received new request from client "+socket.getInetAddress());
                    } else {
                        response.addError("Server received invalid request");
                        logger.writeDebug("Server received invalid request");
                        return;
                    }

                    TransactionDispatcher transactionDispatcher = new TransactionDispatcher();
                    response = transactionDispatcher.ProcessTransaction(request);

                    try (ObjectOutputStream out = SocketUtils.getObjectOutputStream(socket)) {
                        out.writeObject(response);
                        logger.writeDebug("Sent response to client "+socket.getInetAddress());
                    }

                } catch (IOException | ClassNotFoundException ex) {
                    response.addError(ex.getMessage());
                }
            }
        };
    }

}
