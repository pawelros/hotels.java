/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package EntryPoint;

import Communicator.TCPListener;

/**
 *
 * @author pr
 */
public class Main {

    public static void main(String[] args) {
        ConsoleLogger logger = new ConsoleLogger();
        TCPListener listener = new TCPListener(9000, logger);

        try {

            logger.writeDebug("Server is starting...");
            listener.start();

            Thread.sleep(500 * 1000);
            
            logger.writeDebug("Server is stopping...");
            listener.stop();
            logger.writeDebug("Server stopped.");
                       
            
        } catch (Exception ex) {
            logger.writeException(ex);
        }
    }
}
