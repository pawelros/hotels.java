/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package ApiLevel1;

import ApiLevel2.AvailabilityTransaction;
import ApiLevel2.BookTransaction;
import ApiLevel2.PricingTransaction;
import General.Header;
import General.Request;
import General.Response;
import General.TransactionType;

/**
 *
 * @author pr
 */
public class TransactionDispatcher {

    public Response ProcessTransaction(Request request) {

        Header header = request.getHeader();
        TransactionType transactionType = header.getTransactionType();

        switch (transactionType) {
            case AVAILABILITY:
                return new AvailabilityTransaction(request).ProcessTransaction();
            case PRICING:
                return new PricingTransaction(request).ProcessTransaction();
            case BOOK:
                return new BookTransaction(request).ProcessTransaction();
            default:
                throw new UnsupportedOperationException("Transaction is not supported yet.");
        }
    }
}
