/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package Communicator;

import Interfaces.ILogger;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 *
 * @author pr
 */
public class TCPListener {
    
    private final Integer port;
    private final ILogger logger;
    private SocketHandler socketHandler;
    private Boolean stop = false;
    
    public TCPListener(Integer port, ILogger logger) {
        if (port <= 0) {
            throw new IllegalArgumentException("invalid port number " + port);
        }
        this.port = port;
        this.logger = logger;
        this.socketHandler = new SocketHandler(logger);
    }

    /*
     * Public Methods
     */
    public void start() {
        this.logger.writeDebug("Starting TCP Listener...");
        
        Runnable action = listenAction();
        new Thread(action).start();
    }
    
    public synchronized void stop() {
        this.logger.writeDebug("Stopping TCP Listener...");
        this.stop = true;
    }

    /*
     * Private Methods
     */
    private synchronized Boolean isCancellationRequested() {
        return this.stop;
    }
    
    private Runnable listenAction() {
        return new Runnable() {
            
            @Override
            public void run() {
                
                try {
                    listen();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        };
    }
    
    private void listen() throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(this.port)) {
            
            this.logger.writeDebug("Listening on port " + port);
            
            while (!isCancellationRequested()) {
                try {
                    
                    Socket clientSocket = serverSocket.accept();
                    this.socketHandler.Handle(clientSocket);
                    
                } catch (SocketTimeoutException e) {
                    this.logger.writeDebug("Connection timeout");
                    // continue
                }
            }
            
        }
    }
}
