/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package ApiLevel2;

import ApiLevel3.HotelsRetriever;
import General.Content;
import General.Request;
import General.Response;
import java.util.ArrayList;

/**
 *
 * @author pr
 */
public class AvailabilityTransaction implements ITransaction {

    private final Request request;

    public AvailabilityTransaction(Request request) {
        this.request = request;
    }

    @Override
    public Response ProcessTransaction() {

        String city = request.getContent().getCity();
        HotelsRetriever retriever = new HotelsRetriever();
        
        ArrayList<Content> retrievedHotels = retriever.RetrieveByCity(city);
        
        Response response = new Response();
        response.setSupplierResponses(retrievedHotels);
        
        return response;
    }
}
