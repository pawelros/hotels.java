/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package Communicator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author pr
 */
public class SocketUtils {

    public static ObjectInputStream getObjectInputStream(final Socket socket) throws IOException {
          return new ObjectInputStream(socket.getInputStream());
    }
    
    public static ObjectOutputStream getObjectOutputStream(final Socket socket) throws IOException {
        return new ObjectOutputStream(socket.getOutputStream());
    }
    
//    public static PrintWriter getPrintWriter(final Socket socket) throws IOException {
//        return new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"), true);
//    }
//
//    public static BufferedReader getBufferedReader(final Socket socket) throws IOException {
//        return new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
//    }
    
//     private String ReadIn(BufferedReader in) throws IOException{
//        List<String> lines = new ArrayList<>();
//        String line;
//        
//        while((line=in.readLine())!=null){
//            lines.add(line);
//            lines.add("\n");
//        }
//        
//        lines.remove(lines.size()-1);
//        
//        return lines.toString();
//    }
}
