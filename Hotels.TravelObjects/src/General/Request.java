/*
 * Paweł Rosiński 2014
 * WSIIZ Wrocław
 */
package General;

import java.io.Serializable;

/**
 *
 * @author pr
 */
public class Request implements Serializable {

    private Header header;
    private Content content;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }
}
